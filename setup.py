from setuptools import setup

setup(
    name='ii-mssql-lib',
    url='https://gitlab.com/infused-insight/libs/ii_mssql_lib',
    author='Kim Streich',
    author_email='kim@infusedinsight.com',
    packages=['ii_mssql_lib'],
    install_requires=[
        'Jinja2',
        'pyodbc',
    ],
    include_package_data=True,
    version='0.3',
    license='MIT',
    description='Wraper library for pyodbc to interact with MS SQL db.',
)
