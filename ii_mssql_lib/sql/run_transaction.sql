-- run_transaction.sql

BEGIN TRY
    BEGIN TRANSACTION
        SET NOCOUNT ON;

{% for sql_statement in sql_statements %}
        {{ sql_statement | indent(8) }};

{% endfor %}
    COMMIT
END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
        ROLLBACK TRAN

        DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()
        DECLARE @ErrorSeverity INT = ERROR_SEVERITY()
        DECLARE @ErrorState INT = ERROR_STATE()

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
