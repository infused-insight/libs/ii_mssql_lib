-- get_tables.sql

select
    *
from INFORMATION_SCHEMA.TABLES
where 1=1
{% if tables_only is defined and tables_only is true  %}
    and TABLE_TYPE='BASE TABLE'
{% endif %}
{% if views_only is defined and views_only is true  %}
    and TABLE_TYPE='VIEW'
{% endif %}
{% if schema_name is defined and table_name is defined  %}
    and TABLE_SCHEMA='{{ schema_name }}'
    and TABLE_NAME='{{ table_name }}'
{% endif %}
