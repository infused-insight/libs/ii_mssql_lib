-- get_table_column_info.sql

EXEC sp_columns 
    @table_owner = [{{ schema_name }}],
    @table_name = [{{ table_name }}];
