'''
Provides modules to make it easier to work with MS and Azure SQL databases.

For more information...

```
import ii_mssql_lib.mssql_lib
help(ii_mssql_lib.mssql_lib)
```
'''
