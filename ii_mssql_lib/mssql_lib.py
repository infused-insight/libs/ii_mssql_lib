import logging as log
import os
from pathlib import Path
import pyodbc

from jinja2 import Template


class MsSqlLib():
    """
    Wraper library for pyodbc to interact with MS SQL db.

    Supports the use of jinja templated SQL.

    Example:
        ```
        conn = MsSqlLib.init(..., template_searchpath='../sql/')
        jinja_params = {
            'table': 'my_table',
            'where_clause': 'row_num = 1',
        }
        conn.get_records('my_query.sql', jinja_params)
        ```

        The jinja in my_query.sql can then access the `table` and
        `where_clause` variables.
    """
    default_driver = 'ODBC Driver 17 for SQL Server'

    def __init__(self,
                 server,
                 username,
                 password,
                 database=None,
                 port=None,
                 driver=None,
                 template_searchpath=None):
        '''
        Initializes the class with the connection settings.

        Supply `template_searchpath` with a path to a directory with jinja
        templated sql files.

        You can then submit a relative path to your jinja .sql file for the
        `templated_sql` parameters in the get_xxx() methods.
        '''

        self.server = server
        self.username = username
        self.password = password
        self.database = database
        self.port = port
        self.driver = driver

        self._connection = None

        # Add ./sql/ as the default search path for jinja-sql files
        self.template_searchpath = [
            os.path.join(
                os.path.abspath(os.path.dirname(__file__)),
                'sql/',
            ),
        ]

        # And allow the user to add additional search pathes
        if template_searchpath is not None:
            for path in template_searchpath:
                self.template_searchpath.append(
                    os.path.abspath(path)
                )

    def get_records(self, templated_sql, params=None, log_query=True):
        '''
        Executes the sql and returns a set of records.
        '''
        records, _ = self._get_records(
            templated_sql,
            params,
            log_query,
        )

        return records

    def get_records_dict(self, templated_sql, params=None, log_query=True):
        '''
        Executes the sql and returns a set of records as
        a list of dictionaries
        '''
        records, cursor_description = self._get_records(
            templated_sql,
            params,
            log_query,
        )

        if len(records) > 0:
            record_dicts = [
                dict(zip(list(zip(*cursor_description))[0], row))
                for row in records
            ]
        else:
            # Empty list
            record_dicts = records

        return record_dicts

    def get_first(self, templated_sql, params=None, log_query=True):
        '''
        Executes the sql and returns the first resulting row.
        '''
        record, _ = self._get_first(
            templated_sql,
            params,
            log_query,
        )

        return record

    def get_first_dict(self, templated_sql, params=None, log_query=True):
        '''
        Executes the sql and returns the first resulting row as a dictionary.
        '''
        record, cursor_description = self._get_first(
            templated_sql,
            params,
            log_query,
        )

        if record is not None:
            record_dict = dict(
                zip(list(zip(*cursor_description))[0], record)
            )
        else:
            # None
            record_dict = record

        return record_dict

    def run(self, templated_sql, params=None, log_query=True):
        '''
        Runs an SQL command without returning the results.
        '''
        with self.connection as conn:
            with conn.cursor() as cursor:
                self._execute(cursor, templated_sql, params, log_query)
                if log_query is True:
                    log.info(f'Rows affected: {cursor.rowcount}')

    def get_records_as_transaction(self, sql_statements, log_query=True):
        '''
        Accepts a list of `sql_statements` (not templated), wraps them
        in a try/except block, executes it as one SQL command and returns
        the results.

        If an error occurs, all statements are rolled back.
        '''
        sql = self.gen_sql_run_transaction(sql_statements)

        return self.get_records(sql, log_query=log_query)

    def run_as_transaction(self, sql_statements, log_query=True):
        '''
        Accepts a list of `sql_statements` (not templated), wraps them
        in a try/except block and executes it as one SQL command.

        If an error occurs, all statements are rolled back.
        '''
        sql = self.gen_sql_run_transaction(sql_statements)

        self.run(sql, log_query=log_query)

    def parse_jinja_sql(self, templated_sql, params={}):
        '''
        Accepts either a jinja SQL string or a path to a file with jinja SQL.
        The path must end with `.sql` and must be relative to any of the
        pathes within self.template_searchpath.
        '''
        if templated_sql.endswith('.sql') is True:
            templated_sql = self._get_jinja_sql_template_from_file(
                templated_sql
            )

        final_sql = Template(templated_sql).render(**params)

        return final_sql

    def get_tables(self,
                   tables_only=False,
                   views_only=False,
                   log_query=False):
        '''
        Returns a list of dictionaries for each table and view in the DB.

        tables_only=True ensures only tables are returned.
        views_only=True ensures only views are returned.
        '''

        params = {
            'tables_only': tables_only,
            'views_only': views_only,
        }

        tables_info = self.get_records_dict(
            'get_tables.sql',
            params,
            log_query=log_query,
        )

        return tables_info

    def check_if_table_exists(self, schema_name, table_name, log_query=False):
        '''
        Returns True if a table or view with the submitted name exists
        and False if it doesn't.
        '''

        params = {
            'schema_name': schema_name,
            'table_name': table_name,
        }

        tables_info = self.get_records_dict(
            'get_tables.sql',
            params,
            log_query=log_query,
        )

        if len(tables_info) >= 1:
            return True
        else:
            return False

    def get_table_column_info(self,
                              schema_name,
                              table_name,
                              log_query=False):
        '''
        Returns a list of dictionaries for each column of a table.

        Uses `EXEC sp_columns`.
        '''
        params = {
            'schema_name': schema_name,
            'table_name': table_name,
        }

        column_info = self.get_records_dict(
            'get_table_column_info.sql',
            params,
            log_query=log_query,
        )

        if len(column_info) == 0:
            raise MsSqlLibError(
                f'ERROR: Could not determine columns for '
                f'{schema_name}.{table_name}. '
                f'Does the table exist and do you have permissions for it?'
            )

        return column_info

    def get_table_columns(self,
                          schema_name,
                          table_name,
                          log_query=False):
        '''
        Returns the column names of a table as a list.
        '''
        column_info = self.get_table_column_info(
            schema_name,
            table_name,
            log_query
        )

        column_names = [ci['COLUMN_NAME'] for ci in column_info]

        return column_names

    def drop_table(self,
                   schema_name,
                   table_name,
                   log_query=False):
        '''
        Runs a drop table query.
        '''
        params = {
            'schema_name': schema_name,
            'table_name': table_name,
        }

        self.run(
            'drop_table.sql',
            params,
            log_query=log_query,
        )

    def drop_view(self,
                  schema_name,
                  view_name,
                  log_query=False):
        '''
        Runs a drop view query.
        '''
        params = {
            'schema_name': schema_name,
            'view_name': view_name,
        }

        self.run(
            'drop_view.sql',
            params,
            log_query=log_query,
        )

    def truncate_table(self,
                       schema_name,
                       table_name,
                       log_query=False):
        '''
        Runs a truncate table query.
        '''
        params = {
            'schema_name': schema_name,
            'table_name': table_name,
        }

        self.run(
            'truncate_table.sql',
            params,
            log_query=log_query,
        )

    def gen_sql_run_transaction(self,
                                sql_statements):
        '''
        Accepts a list of `sql_statements` (not templated) and wraps them
        in a try/except block that rolls back all statements if an error
        occurs.
        '''
        if type(sql_statements) is not list:
            sql_statements = [sql_statements]

        # Trim whitespace
        # Remove trailing ';', because it is added in the template
        formatted_sql_statements = []
        for s in sql_statements:
            s = s.strip()
            if s[-1] == ';':
                s = s[:-1]
            formatted_sql_statements.append(s)

        params = {
            'sql_statements': formatted_sql_statements,
        }

        sql = self.parse_jinja_sql(
            'run_transaction.sql',
            params,
        )

        return sql

    @property
    def connection(self):
        if self._connection is None:
            conn_str = self.create_connection_string(
                driver=self.driver,
                server=self.server,
                port=self.port,
                database=self.database,
                username=self.username,
                password=self.password,
            )
            self._connection = pyodbc.connect(conn_str)
        return self._connection

    def create_connection_string(self,
                                 server,
                                 username,
                                 password,
                                 database=None,
                                 port=None,
                                 driver=None):
        driver = driver or self.default_driver

        conn_str_elements = [
            f'DRIVER={{{driver}}}',
            f'SERVER={{{server}}}',
            f'UID={{{username}}}',
            f'PWD={{{password}}}',
        ]

        if database is not None:
            conn_str_elements.append(
                f'DATABASE={{{database}}}',
            )

        if port is not None:
            conn_str_elements.append(
                f'PORT={port}',
            )

        conn_str = ';'.join(conn_str_elements)

        return conn_str

    def _get_records(self, templated_sql, params=None, log_query=True):
        with self.connection as conn:
            with conn.cursor() as cursor:
                self._execute(cursor, templated_sql, params, log_query)
                return cursor.fetchall(), cursor.description

    def _get_first(self, templated_sql, params=None, log_query=True):
        with self.connection as conn:
            with conn.cursor() as cursor:
                self._execute(
                    cursor,
                    templated_sql,
                    params,
                    log_query,
                )
                return cursor.fetchone(), cursor.description

    def _execute(self, cursor, templated_sql, params=None, log_query=True):
        if params is None:
            params = {}

        final_sql = self.parse_jinja_sql(
            templated_sql,
            params,
        )

        if log_query is True:
            log.info(
                f'Executing SQL:\n'
                f'{final_sql}'
            )

        cursor.execute(final_sql)

    def _get_jinja_sql_template_from_file(self, template_path):
        found_path = None
        for search_path in self.template_searchpath:
            file_path_full = self._check_template_file_exists(
                search_path,
                template_path
            )

            if file_path_full is not None:
                found_path = file_path_full
                break

        if found_path is None:
            allowed_paths = ', '.join(self.template_searchpath)
            raise MsSqlLibError(
                f'Could not find template file `{template_path}`, in the '
                f'allowed template search paths: {allowed_paths}.'
            )

        template_content = Path(found_path).read_text()

        return template_content

    def _check_template_file_exists(self, search_path, template_path):
        '''
        Checks if the file exists relative to the search_path and does
        not attempt directory traversal.

        Returns the full, absolute path if the file exists and None otherwise.
        '''
        search_path_abs = os.path.abspath(search_path)
        template_path_full = os.path.abspath(
            os.path.join(
                search_path_abs,
                template_path,
            )
        )
        self._path_check_directory_traversal(
            search_path_abs,
            template_path_full,
        )

        if os.path.isfile(template_path_full) is False:
            return None

        return template_path_full

    def _path_check_directory_traversal(self, allowed_dir, path):
        path_abs = os.path.abspath(path)
        common_prefix = os.path.commonprefix([allowed_dir, path_abs])
        if common_prefix != allowed_dir:
            allowed_paths = ', '.join(self.template_searchpath)
            raise MsSqlLibError(
                f"Cannot use template file `{path}`, because it's not in "
                f"the allowed template search paths: {allowed_paths}. "
                f"Use the `template_searchpath` param to add additional "
                f"pathes."
            )

        return True


class MsSqlLibError(Exception):
    def __init__(self, msg):
        Exception.__init__(self, msg)
